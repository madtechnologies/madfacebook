<?php

/**
 * Component para el manejo de llamadas a Facebook Api
 */

/**
 * Import
 * - Vendor/Facebook
 */
App::import("Vendor", "Facebook", ['file' => 'Facebook/src/Facebook/autoload.php']);

/**
 * Uses
 * - Controller/Components/Component
 */
App::uses("Component", "Controller/Component");

/**
 * Date 2015-10-29
 * @version 0.1
 * @since 0.1
 * @package Facebook.Controller
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class FbComponent extends Component {

	/**
	 *
	 */
	private static $fbApp, $fb;

	/**
	 *
	 */
 	public function initialize(Controller $controller) {
 		parent::initialize($controller);
		self::$fb = new Facebook\Facebook([
			'app_id' => Configure::read("Facebook.appId"),
			'app_secret' => Configure::read("Facebook.appSecret"),
			'default_graph_version' => 'v2.5'
		]);
    }	

	/**
	 *
	 */
 	public static function get($url, $accessToken) {
		$response = self::$fb->get($url, $accessToken);
		$factory = new Facebook\GraphNodes\GraphNodeFactory($response);
		return $factory->makeGraphNode()->uncastItems();
	}

	/**
	 *
	 */
 	public static function getCollection($url, $accessToken) {
		$response = self::$fb->get($url, $accessToken);
		$factory = new Facebook\GraphNodes\GraphNodeFactory($response);
		return $factory->makeGraphEdge();
	}

	/**
	 *
	 */
	public function getRedirectLoginHelper() {
		return self::$fb->getRedirectLoginHelper();
	}

	/**
	 *
	 */
	public function loginUrl($permissions = ['email']) {
		$helper = self::$fb->getRedirectLoginHelper();
		return $helper->getLoginUrl(Router::url(['plugin' => 'facebook', 'controller' => 'facebook', 'action' => 'callback'], true), $permissions);
	}

}