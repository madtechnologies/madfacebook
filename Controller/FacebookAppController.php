<?php

/**
 * Uses: 
 * - Controller/AppController
 */
App::uses('AppController', 'Controller');

/**
 * Controller inicial del Plugin Facebook
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Facebook.Controller
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class FacebookAppController extends AppController {

}
