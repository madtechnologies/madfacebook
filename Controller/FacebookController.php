<?php

/**
 * Uses: 
 * - Network/Email/CakeEmail
 * - Facebook.Controller/FAcebookAppController
 */
App::uses('CakeEmail', 'Network/Email');
App::uses("FacebookAppController", "Facebook.Controller");

/**
 * Import
 * - Vendor/Facebook
 */
App::import("Vendor", "Facebook", ['file' => 'Facebook/src/Facebook/autoload.php']);

/**
 * Controller manejo de usuarios con Facebook
 * @version 0.1.1
 * @since 0.1
 * @package Facebook.Controller
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class FacebookController extends FacebookAppController {

	/**
	 * Seteo los accesos publicos del controller
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(['callback', 'activate']);
	}

	/**
	 * 
	 */
	public function activate($userId = null) {
		$user = $this->Facebook->User->find('first', ['conditions' => ['User.id' => $userId, 'User.active' => false]]);
		if(empty($user)) {
			$this->Session->setFlash("Codigo de Activación Incorrecto o el Usuario ya esta activo");
		} else {
			$ds = $this->Facebook->User->getDataSource();
			$ds->begin();
			$this->Facebook->User->id = $userId;
			$this->Facebook->User->set("active", true);
			$user = $this->Facebook->User->save();
			if($user) {
				$facebook = $this->Facebook->findByUserId($userId);
				$this->Facebook->id = Hash::get($facebook, "Facebook.id");
				$this->Facebook->set("active", true);
				$facebook = $this->Facebook->save();
				if($facebook) {
					$ds->commit();
					$this->redirect(FbComponent::loginUrl(['email', 'manage_pages']));
				}
			}
			$ds->rollback();
			$this->Session->setFlash("El usuario no pudo activarse. Intente nuevamente.");
		}
		$this->redirect(DS);
	}

	/**
	 * Action callback para la coneccion oauth con Facebook
	 */
	public function callback() {

		$helper = FbComponent::getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();  
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		if (!isset($accessToken)) {
			throw new Exception($e->getMessage());
		}
		try {
			$response = FbComponent::get("/me?fields=name,email,id", $accessToken);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());	
		}

		$name = $response['name'];
		$email = $response['email'];
		$facebookId = $response['id'];

		$facebook = $this->Facebook->findByFacebookId($facebookId);

		if($facebook) {

			if(Hash::get($facebook, "Facebook.active") && !Hash::get($facebook, "Facebook.is_fanpage")) {
	
				$this->Facebook->User->contain(['Group', 'FacebookUser']);
				$user = $this->Facebook->User->read(null, Hash::get($facebook, 'Facebook.user_id'));

				$user = Hash::merge(Hash::extract($user, "User"), $user);
				unset($user['User']);

				$this->Auth->login($user);
				$this->Session->setFlash("Bienvenido!");
				$this->redirect("/profile");

			} else {
				$this->Session->setFlash("Usuario inactivo!");
			}

		} else {

			$user = $this->Facebook->User->findByUsername($email);

			if(!is_null(Hash::get($user, "User.deleted"))) {
				$this->Session->setFlash("Usuario ha sido borrado de la plataforma");
				$this->redirect(DS);
			}

			$this->Facebook->getDataSource()->begin();

			if(!$user) {

				$user = $this->Facebook->User->createNew($email);

				if(!$user) {
					$this->Facebook->getDataSource()->rollback();
					$this->Session->setFlash("Error en la creacion del Usuario");
					$this->redirect(DS);
				}

			}

			$facebook = $this->Facebook->createNew(
				Hash::get($user, "User.id"),
				$facebookId,
				$accessToken,
				$name
			);

			if($facebook) {

				$this->Facebook->getDataSource()->commit();

				try {
					$email = new CakeEmail();
					$email->to(Hash::get($user, "User.username"));
					$email->subject("Registracion en ".Configure::read("Mad.brandName"));
					$email->emailFormat('html');
					$email->theme(Configure::read("ThemeWeb"));
					$email->template('Facebook.register', 'default');
					$email->viewVars(array('user' => $user));
					$email->send();
				} catch(Exception $e) {
					throw new Exception($e->getMessage());	
				}
				$this->Session->setFlash("Chequea tu email y activa tu cuenta!");

			} else {
				$this->Facebook->getDataSource()->rollback();
				$this->Session->setFlash("Error en la creacion del Usuario");
			}

		}
		$this->redirect(DS);
	}

}