<?php

/**
 *
 */

/**
 *
 */
App::uses("FacebookAppModel", "Facebook.Model");

/**
 * Date 2015-10-29
 * @version 0.1
 * @since 0.1
 * @package Facebook.Model
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class Facebook extends FacebookAppModel {

	/**
	 *
 	 */
	public $useTable = "facebook";

	/**
	 *
	 */
	public $belongsTo = [
		'User' => ['className' => 'Users.User']
	];

	/**
	 *
	 */
	public function createNew($userId, $facebookId, $accessToken, $name, $active = false) {
		$this->create();
		$this->set("user_id", $userId);
		$this->set("facebook_id", $facebookId);
		$this->set("access_token", $accessToken);
		$this->set("name", $name);
		$this->set("active", $active);
		return $this->save();
	}

	/**
	 *
	 */
 	public function registerFanpage($facebookId, $accessToken, $name) {
		$this->create();
		$this->set("user_id", AuthComponent::user("id"));
		$this->set("facebook_id", $facebookId);
		$this->set("access_token", $accessToken);
		$this->set("name", $name);
		$this->set("active", true);
		$this->set("is_fanpage", true);
		return $this->save();
	}

	public function fanpageExist($fanpageId) {
		return $this->find("first", ['conditions' => ['Facebook.is_fanpage' => true, 'Facebook.facebook_id' => $fanpageId]]);
	}

	/**
	 *
	 */
	public function fanpageAvailable($id) {
		return $this->find("first", ['conditions' => ['Facebook.deleted' => null, 'Facebook.active' => true, 'Facebook.is_fanpage' => true, 'Facebook.id' => $id]]);
	}

}