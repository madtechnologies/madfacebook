# README #

MadFacebook es un Plugin que conecta tu CakePHP con la api de Facebook

### Descripción ###

* MadFacebook es un Plugin de CakePHP que conecta tu app con la api de Facebook.
* Version 0.1

### Configuración ###

* Utiliza el Plugin de Users y la libreria de Facebook PHP
* Poner en el bootstrap.php de la app las siguientes lineas para conectar con la app de Facebook:
```PHP
<?php
Configure::write("Facebook.appId", "FACEBOOKAPPID");
Configure::write("Facebook.appSecret", "FACEBOOKAPPSECRET");
?>
```

### Contribution guidelines ###

* Hernan Iglesias: hernan.iglesias@madtechnologies.com.ar]