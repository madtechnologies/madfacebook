<h1>Registracion con Facebook en <?php echo Configure::read("Mad.brandName")?></h1>
<dl>
	<dt>Para activar tu cuenta hace click en el siguiente enlace:</dt>
	<dd>
		<?php 
		$activationCode = Hash::get($user, "User.activation_code");
		$id = Hash::get($user, "User.id");
		echo $this->Html->link("Activar Cuenta", ['plugin' => 'facebook', 'controller' => 'facebook', 'action' => 'activate', 'prefix' => false, 'admin' => false, $id, 'full_base' => true]);
		?>
	</dd>
</dl>